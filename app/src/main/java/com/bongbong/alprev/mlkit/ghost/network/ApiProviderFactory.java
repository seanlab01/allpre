package com.bongbong.alprev.mlkit.ghost.network;

public interface ApiProviderFactory {

    ApiProvider create(String blogUrl);

}
