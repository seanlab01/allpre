package com.bongbong.alprev.mlkit.ghost.util.functions;

/**
 * A zero-argument action.
 */
public interface Action0 {

    void call();

}
