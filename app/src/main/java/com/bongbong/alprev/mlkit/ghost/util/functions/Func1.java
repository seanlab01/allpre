package com.bongbong.alprev.mlkit.ghost.util.functions;

/**
 * A function with one argument.
 */
public interface Func1<T, R> {

    R call(T t);

}
