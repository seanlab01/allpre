package com.bongbong.alprev.mlkit.ghost.util.functions;

/**
 * A one-argument action.
 */
public interface Action1<T> {

    void call(T t);

}
