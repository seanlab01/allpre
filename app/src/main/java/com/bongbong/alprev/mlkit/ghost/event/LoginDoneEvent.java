package com.bongbong.alprev.mlkit.ghost.event;

public class LoginDoneEvent {

    public final String blogUrl;

    public LoginDoneEvent(String blogUrl) {
        this.blogUrl = blogUrl;
    }

}
